package com.example.retrofitrequest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class NetworkService {
    private  static NetworkService mInstance;
    private  static final String BASE_URL = "https://api.github.com/";
    private final Retrofit mRetrofit;

    public static NetworkService getInstance() {

        synchronized (NetworkService.class){
            if (mInstance == null) {
                mInstance = new NetworkService();
            }
            return mInstance;
        }

    }

    public NetworkService() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public GithubAPI getJSONApi() {
        return mRetrofit.create(GithubAPI.class);
    }

}
