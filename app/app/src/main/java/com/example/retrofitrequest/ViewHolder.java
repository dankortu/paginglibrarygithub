package com.example.retrofitrequest;


import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitrequest.databinding.UserBinding;

public class ViewHolder<M extends RegularUser> extends RecyclerView.ViewHolder {

    private UserBinding binding;
    private final Adapter.OnUserClickCallback<M> callback;
    private M currentUser;

    public ViewHolder(@NonNull UserBinding userView, @NonNull Adapter.OnUserClickCallback<M> callback) {
        super(userView.getRoot());
        this.binding = userView;
        this.callback = callback;
    }


    public void bind(M user) {
        this.currentUser = user;
        binding.getRoot().setOnClickListener(this::handleClick);
        binding.login.setText(user.getLogin());
        binding.id.setText(user.getId());
        binding.id.setSelected(true);
        binding.nodeId.setText(user.getNodeId());

        binding.getRoot().setBackground(
                ContextCompat.getDrawable(binding.getRoot().getContext(),
                        Integer.parseInt(user.getId()) % 2 != 0 ?
                                R.drawable.ripple
                                : R.drawable.ripple_gragient)
        );

    }

    private void handleClick(View view) {
        callback.OnClick(currentUser, getAdapterPosition());
    }
}
