package com.example.retrofitrequest;

public interface OnItemClickCallback {
    void OnClick(User user, int adapterPosition);
}
