package com.example.retrofitrequest;

import java.util.ArrayList;
import java.util.List;

public class Storage<M extends RegularUser> implements StorageInterface<M> {
    public List<M> storageUsers = new ArrayList<>();
    private Adapter<M> adapter;
    private final OnLoadCallback<M> callback;
    private boolean loading;

    @Override
    public void setAdapter(Adapter<M> adapter) {
        this.adapter = adapter;
    }


    Storage(OnLoadCallback<M> callback) {
        this.callback = callback;
    }

    @Override
    public M getItem(int position) {

        if (!loading) {
            if (position >= storageUsers.size() - 50) {
                loading = true;
                callback.makeRequest(usersList -> {
                    storageUsers.addAll(usersList);
                    adapter.notifyDataSetChanged();
                    loading = false;
                }, getLastId());
            }
        }

        return storageUsers.get(position);
    }

    private String getLastId() {
        return storageUsers.size() == 0
                ? String.valueOf(0)
                : storageUsers.get(storageUsers.size() - 1).getId();
    }

    @Override
    public int getSize() {
        return storageUsers.size();
    }


    public void setStorageItems(List<M> storageUsers) {
        this.storageUsers = storageUsers;
    }


    @Override
    public List<M> getStorageItems() {
        return storageUsers;
    }


    public interface OnLoadCallback<M> {
        void makeRequest(OnLoadCSuccess<M> successCallback, String id);
    }

    public interface OnLoadCSuccess<M> {
        void getResult(List<M> usersList);
    }

}
