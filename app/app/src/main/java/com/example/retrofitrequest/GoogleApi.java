package com.example.retrofitrequest;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface GoogleApi {
    @GET("/users?per_page=100&access_token=0771a14aa32defaf454647a9775fbf2165668680")
    Call<List<User>> getPostWithUsers(
            @Query("since") String userId
    );
}
