package com.example.retrofitrequest;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFragment extends Fragment {

    private static final String USERS_KEY = "USERS_KEY";
    private static final String USERS_REQUEST_KEY = "USERS_REQUEST_KEY";

    private List<User> users;
    private Adapter<User> adapter;

    public MyFragment(){};

    public static MyFragment newInstance(List<RegularUser> users) {
        MyFragment fragment = new MyFragment();
        final Bundle args = new Bundle();
        args.putParcelableArrayList(USERS_KEY, (ArrayList<? extends Parcelable>) users);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        final Bundle arguments = getArguments();
        if (arguments != null) {
            initFragment(arguments);
        }
    }

    private void initFragment(Bundle arguments) {
        users = new ArrayList<>();
        for (Parcelable parcelable: arguments.getParcelableArrayList(USERS_KEY)) {
            users.add((User) parcelable);
        }

        Storage<User> storageForAdapter = new Storage<>((callback, id) ->
                NetworkService.getInstance().getJSONApi().getPostWithUsers(id).enqueue(new Callback<List<User>>() {

                    @Override
                    public void onResponse(@NonNull Call<List<User>> call, @NonNull Response<List<User>> response) {
                        List<User> usersList = response.body();
                        callback.getResult(usersList);
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<User>> call, @NonNull Throwable t) {
                    }

                })

        );

        adapter = new Adapter<>((user, adapterPosition) -> {
            if (isAdded()){
                users.remove(user);

                final Bundle newUser = new Bundle();
                newUser.putString("login", user.getLogin());
                newUser.putString("id", user.getId());
                newUser.putString("nodeId", user.getNodeId());


                getParentFragmentManager().setFragmentResult(USERS_REQUEST_KEY, newUser);
            }


            storageForAdapter.setStorageItems(users);
            adapter.setStorage(storageForAdapter);
        });


        storageForAdapter.setAdapter(adapter);
        storageForAdapter.setStorageItems(users);
        adapter.setStorage(storageForAdapter);

        getParentFragmentManager().setFragmentResultListener(USERS_REQUEST_KEY, this, (requestKey, newUser) -> {
            String login = newUser.getString("login");
            String id = newUser.getString("id");
            String nodeId = newUser.getString("nodeId");

            users.add(new User(login, id, nodeId));
            storageForAdapter.setStorageItems(users);
            adapter.setStorage(storageForAdapter);
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recycler = view.findViewById(R.id.recycler);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(requireContext()));
    }
}

//                (Storage.OnLoadCallback<RegularUser>) callback -> {
//                  callback.onLoadSuccess();
//                }
//                () -> {

//        }
