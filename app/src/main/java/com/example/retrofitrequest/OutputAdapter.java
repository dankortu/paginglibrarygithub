package com.example.retrofitrequest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitrequest.databinding.UserBinding;

import java.util.List;

public class OutputAdapter<M extends RegularUser> extends RecyclerView.Adapter<ViewHolder<M>> {
    private List<M> items;
    private OnItemClickCallback callback;

    public OutputAdapter(List<M> items, OnItemClickCallback callback) {
        this.items = items;
        this.callback = callback;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder<M> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder<>(UserBinding.inflate(inflater, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<M> holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
