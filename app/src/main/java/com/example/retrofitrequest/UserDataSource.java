package com.example.retrofitrequest;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataSource extends PageKeyedDataSource<Long, User> {

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, User> callback) {
        NetworkService.getInstance().getJSONApi().getPostWithUsers("0").enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(@NonNull Call<List<User>> call, @NonNull Response<List<User>> response) {
                List<User> usersList = response.body();
                assert usersList != null;
                callback.onResult(usersList, null, Long.parseLong(usersList.get(usersList.size()-1).getId()));
            }

            @Override
            public void onFailure(@NonNull Call<List<User>> call, @NonNull Throwable t) {

                Log.i("RESPONSE", "onFailure: " + t.getMessage());
            }

        });

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, User> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, User> callback) {

        NetworkService.getInstance().getJSONApi().getPostWithUsers(params.key.toString()).enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(@NonNull Call<List<User>> call, @NonNull Response<List<User>> response) {
                List<User> usersList = response.body();
                assert usersList != null;
                callback.onResult(usersList, Long.parseLong(usersList.get(usersList.size()-1).getId()));
            }

            @Override
            public void onFailure(@NonNull Call<List<User>> call, @NonNull Throwable t) {

                Log.i("RESPONSE", "onFailure: " + t.getMessage());
            }

        });


    }
}

