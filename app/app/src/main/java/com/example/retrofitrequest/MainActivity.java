package com.example.retrofitrequest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.example.retrofitrequest.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        final ActivityMainBinding viewBinding = ActivityMainBinding.inflate(getLayoutInflater());
        final List<Fragment> fragments = new ArrayList<>(getSupportFragmentManager().getFragments());
        List<RegularUser> users = new ArrayList<>();


        setContentView(viewBinding.getRoot());

        NetworkService.getInstance().getJSONApi().getPostWithUsers("0").enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(@NonNull Call<List<User>> call, @NonNull Response<List<User>> response) {
                List<User> usersList = response.body();

                for (User user : usersList) {
                    users.add(new User(user.getLogin(), user.getId(), user.getNodeId()));
                }

                if (fragments.isEmpty()) {
                    List<RegularUser> outputList = new ArrayList<>();
                    fragments.add(MyFragment.newInstance(users));
                    fragments.add(MyFragment.newInstance(outputList));
                }

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    MainPagesAdapter adapter = new MainPagesAdapter(
                            getSupportFragmentManager(),
                            FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                            fragments
                    );



                    viewBinding.viewPager.setAdapter(adapter);
                    viewBinding.tabLayout.setupWithViewPager(viewBinding.viewPager);
                }

            }

            @Override
            public void onFailure(@NonNull Call<List<User>> call, @NonNull Throwable t) {

                Log.i("RESPONSE", "onFailure: " + t.getMessage());
            }

        });

    }

}