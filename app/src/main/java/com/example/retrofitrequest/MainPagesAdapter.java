package com.example.retrofitrequest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MainPagesAdapter extends FragmentPagerAdapter {

    private final List<Fragment> initialFragments;

    public MainPagesAdapter(@NonNull FragmentManager fm, int behavior, List<Fragment> fragments) {
        super(fm, behavior);
        this.initialFragments = fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return initialFragments.get(position);
    }

    @Override
    public int getCount() {
        return initialFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "INPUT";
        } else {
            return "OUTPUT";
        }

    }
}
