package com.example.retrofitrequest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.retrofitrequest.databinding.UserBinding;

public class UserAdapter extends androidx.paging.PagedListAdapter<User, ViewHolder<User>> {

    private final OnItemClickCallback callback;

    public UserAdapter(OnItemClickCallback callback) {
        super(User.CALLBACK);
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder<User> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder<>(UserBinding.inflate(inflater, parent, false),  callback);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<User> holder, int position) {
        User user = getItem(position);
        assert user != null;
        holder.bind(user);
    }

}



