package com.example.retrofitrequest;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UserViewModel extends AndroidViewModel {

    DataSource.Factory<Long, User> userDataSourceFactory;
    Executor executor;
    LiveData<PagedList<User>> pagedListLiveData;

    PagedList pagedList;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userDataSourceFactory = new DataSource.Factory<Long, User>() {
            @NonNull
            @Override
            public DataSource<Long, User> create() {
                return new UserDataSource();
            }
        };

        PagedList.Config config = (new PagedList.Config.Builder())
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(10)
                .setPrefetchDistance(10)
                .build();
        executor = Executors.newFixedThreadPool(5);

//        pagedList = (new PagedList.Builder(userDataSourceFactory.create(), config)).setFetchExecutor(executor).build();
        pagedListLiveData = (new LivePagedListBuilder<>(userDataSourceFactory, config))
                .setFetchExecutor(executor)
                .build();
    }

    public LiveData<PagedList<User>> getPagedListLiveData() {
        return pagedListLiveData;
    }

    public PagedList getPagedList() {
        return pagedList;
    }

}
