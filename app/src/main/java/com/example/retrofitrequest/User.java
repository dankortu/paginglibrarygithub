package com.example.retrofitrequest;
import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User implements RegularUser {
    @SerializedName("login")
    @Expose
    private final String login;
    @SerializedName("id")
    @Expose
    private final String id;
    @SerializedName("node_id")
    @Expose
    private final String nodeId;

    public User(String login, String id, String nodeId) {
        this.login = login;
        this.id = id;
        this.nodeId = nodeId;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(Parcel in) {
        login = in.readString();
        id = in.readString();
        nodeId=in.readString();
    }


    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getNodeId() {
        return nodeId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(id);
        dest.writeString(nodeId);
    }

    public static final DiffUtil.ItemCallback<User> CALLBACK = new DiffUtil.ItemCallback<User>() {
        @Override
        public boolean areItemsTheSame(@NonNull User oldUser, @NonNull User newUser) {
            return oldUser.id == newUser.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull User oldUser, @NonNull User newUser) { //а если id совпадает а данные нет?
            return true;
        }
    };

}
