package com.example.retrofitrequest;



import java.util.List;

public interface StorageInterface<M extends RegularUser> {
    void setStorageItems(List<M> storageUsers);
    void setAdapter(Adapter<M> adapter);
    List<M> getStorageItems();
    M getItem(int position);

    int getSize();
}
