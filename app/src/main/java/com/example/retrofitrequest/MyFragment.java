package com.example.retrofitrequest;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFragment extends Fragment {

    private static final String USERS_REQUEST_KEY = "USERS_REQUEST_KEY";
    Bundle arguments;

    UserViewModel userViewModel;
    OutputAdapter outputAdapter;
    List<User> outputUsers = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arguments = getArguments();
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
//        userViewModel = ViewModelProvider.AndroidViewModelFactory.;

        getParentFragmentManager().setFragmentResultListener(USERS_REQUEST_KEY, this, (requestKey, newUser) -> {

            String login = newUser.getString("login");
            String id = newUser.getString("id");
            String nodeId = newUser.getString("nodeId");

            outputUsers.add(new User(login, id, nodeId));
            if (outputAdapter != null) {
                outputAdapter.notifyDataSetChanged();
            }
        });

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

/*        userViewModel.getPagedList().addWeakCallback(new List<User>, users -> {
            RecyclerView recycler = view.findViewById(R.id.recycler);
            recycler.setLayoutManager(new LinearLayoutManager(requireContext()));

            if (arguments.getString("NAME_OF_FRAGMENT").equals("INPUT")) {

                UserAdapter userAdapter = new UserAdapter((user, adapterPosition) -> {
                    if (isAdded()) {
                        final Bundle newUser = new Bundle();
                        newUser.putString("login", user.getLogin());
                        newUser.putString("id", user.getId());
                        newUser.putString("nodeId", user.getNodeId());

                        getParentFragmentManager().setFragmentResult(USERS_REQUEST_KEY, newUser);
                    }

                });
                userAdapter.submitList(users);
                userAdapter.submitList(users);
                recycler.setAdapter(userAdapter);

            } else {
                outputAdapter = new OutputAdapter<>(outputUsers, (user, adapterPosition)->{});
                recycler.setAdapter(outputAdapter);
            }
        });*/

        userViewModel.getPagedListLiveData().observe(getViewLifecycleOwner(), users -> {

            RecyclerView recycler = view.findViewById(R.id.recycler);
            recycler.setLayoutManager(new LinearLayoutManager(requireContext()));

            if (arguments.getString("NAME_OF_FRAGMENT").equals("INPUT")) {

                UserAdapter userAdapter = new UserAdapter((user, adapterPosition) -> {
                    if (isAdded()) {
                        final Bundle newUser = new Bundle();
                        newUser.putString("login", user.getLogin());
                        newUser.putString("id", user.getId());
                        newUser.putString("nodeId", user.getNodeId());

                        getParentFragmentManager().setFragmentResult(USERS_REQUEST_KEY, newUser);
                    }

                });
                userAdapter.submitList(users);
                recycler.setAdapter(userAdapter);

            } else {
                outputAdapter = new OutputAdapter<>(outputUsers, (user, adapterPosition)->{});
                recycler.setAdapter(outputAdapter);
            }
        });

    }

}

