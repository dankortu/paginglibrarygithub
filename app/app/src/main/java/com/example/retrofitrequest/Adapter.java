package com.example.retrofitrequest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitrequest.databinding.UserBinding;

import java.util.ArrayList;
import java.util.List;


public class Adapter<M extends RegularUser> extends RecyclerView.Adapter<ViewHolder<M>> {
    private final OnUserClickCallback<M> callback;
    private Storage<M> storage;

    public Adapter(OnUserClickCallback<M> callback) {
        this.callback = callback;
    }

    public void setStorage(Storage<M> storage){
        this.storage = storage;
    }


    @NonNull
    @Override
    public ViewHolder<M> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder<>(UserBinding.inflate(inflater, parent, false), callback); // конструктор во viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder<M> holder, int position) {
        M user = storage.getItem(position);
        holder.bind(user);
    }


    @Override
    public int getItemCount() {
        return storage.getSize();
    }


    public interface OnUserClickCallback<M> {
        void OnClick(M user, int adapterPosition);
    }



}
