package com.example.retrofitrequest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.example.retrofitrequest.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final List<Fragment> fragments = new ArrayList<>(getSupportFragmentManager().getFragments());
        super.onCreate(savedInstanceState);
        final ActivityMainBinding viewBinding = ActivityMainBinding.inflate(getLayoutInflater());

        setContentView(viewBinding.getRoot());

        if (fragments.isEmpty()) {

            MyFragment input = new MyFragment();
            Bundle inputBundle = new Bundle();
            inputBundle.putString("NAME_OF_FRAGMENT", "INPUT");
            input.setArguments(inputBundle);

            MyFragment output = new MyFragment();
            Bundle outputBundle = new Bundle();
            outputBundle.putString("NAME_OF_FRAGMENT", "OUTPUT");
            output.setArguments(outputBundle);

            fragments.addAll(Arrays.asList(input, output));
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            MainPagesAdapter adapter = new MainPagesAdapter(
                    getSupportFragmentManager(),
                    FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                    fragments
            );


            viewBinding.viewPager.setAdapter(adapter);
            viewBinding.tabLayout.setupWithViewPager(viewBinding.viewPager);
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.input, fragments.get(0)).replace(R.id.output, fragments.get(1)).commit();
        }

    }

}