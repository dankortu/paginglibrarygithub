package com.example.retrofitrequest;

import android.os.Parcelable;

public interface RegularUser extends Parcelable {
 String getLogin();
 String getId();
 String getNodeId();
}
