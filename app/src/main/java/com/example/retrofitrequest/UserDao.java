package com.example.retrofitrequest;

import androidx.paging.DataSource;

import retrofit2.http.Query;

public interface UserDao {
//    @Query("SELECT * FROM users ORDER BY date DESC")
    DataSource.Factory<Integer, User> usersById();
}
